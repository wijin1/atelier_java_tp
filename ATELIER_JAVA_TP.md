
# TP - Atelier de programmation Java

Dans une pizzeria se spécialise dans les pizzas personnalisées.

Dans l'énoncé les prix annoncés sont les prix hors taxe (sans TVA).

## Création de la Pizzeria - 1pt

Créer un package pizzeria qui contiendra l'ensemble de vos classes.

Créer une **classe Pizzeria**, il s'agira du point d'entrée du programme (méthode main).

## Pâte - 1pt

Créer une **énumération PatePizza** qui correspond au type de la pâte. Chaque pâte est associée à un coefficient qui permettra de calculer le prix de la garniture et de son propre prix.
- CLASSIQUE, coefficient de la garniture : 1, prix de la pâte : 3€
- CLASSIQUE_DOUBLE, coefficient de la garniture : 2, prix de la pâte : 5€
- EPAISSE, coefficient de la garniture : 1.5, prix de la pâte : 4€
- EPAISSE_DOUBLE, coefficient de la garniture : 3, prix de la pâte : 7€

L'unité de coût est l'Euro (€).

## Garniture - 1pt

Créer une **énumération ElementGarniture** qui contient les ingrédients d'une Pizza avec le prix hors-taxe de ces ingrédients pour une pizza simple:

- OIGNON à 0.5€
- ANCHOIS à 1.5€
- FROMAGE à 1€,
- OLIVE à 0.75€,
- TOMATE à 0.2€,
- OEUF à 0.25€,
- VIANDE_HACHEE à 2€,
- MERGUEZ à 1€,
- ANANAS à 1€,
- GESIER à 0.4€.

## Création du modèle de pizza - 1pt

Créer un **interface IPizza** qui permet d'obtenir les informations suivantes de la Pizza :
- le type de pâte (**PatePizza**), méthode **getPate**
- le tableau des ingrédients qui composent la garnitue de la Pizza (**ElementGarniture[]**), méthode **getGarniture**


## La Pizza - 2pt

Créer une classe **abstraite APizza** qui prend en paramères de son constructeur le type de pâte et le tableau des ingrédients de la pizza.
Cette classe abstraite doit implémenter l'**interface IPizza** et définir une méthode non abstraite qui permet de calculer et de retourner le prix d'une **APizza** à partir de sa composition
et du taux de TVA passé en paramètre, méthode **calculCout** :


`((somme des prix HT des ingrédients) * coefficient de la pâte + coût de la pâte) * taux TVA`

Les attributs ne doivent pas être modifiables après la construction d'une **APizza**.

Rajouter les getters nécessaires dans vos énumérations.

## Les classiques - 2pt

Créer les classes de pizzas suivantes qui héritent de la classe mère **APizza** :
- **PizzaClassiqueVegetarienne** : OIGNON, OLIVE, TOMATE, OEUF, FROMAGE
- **PizzaClassiqueVegan** : OIGNON, OLIVE, TOMATE, ANANAS
- **PizzaClassiqueCarnasiere** : OEUF, VIANDE_HACHEE, MERGUEZ, TOMATE, FROMAGE
- **PizzaClassiqueOceanique** : OIGNON, FROMAGE, TOMATE, ANCHOIS

Contrairement aux ingrédients, il doit être possible de choisir la pate de la pizza **PatePizza**.

## La personnalisée - 1pt

Créer une classe **PizzaPersonnalisee** qui hérite de la classe mère **APizza**.
Cette pizza doit laisser le choix de la pate et de la garniture.
En raison du temps passé à prendre la commande, le prix de la pizza doit être majoré de 1€ (réutiliser la méthode de calcul du coût des autres pizzas).

## L'addition s'il vous plait - 1pt

Dans la **classe Pizzeria**, instancier les pizzas suivantes et afficher leurs prix TTC pour un taux de TVA de 20% :
 - PizzaClassiqueVegetarienne avec une pate CLASSIQUE
 - PizzaClassiqueVegan avec une pate CLASSIQUE_DOUBLE
 - PizzaClassiqueCarnasiere avec une pate EPAISSE
 - PizzaClassiqueOceanique avec une pate EPAISSE_DOUBLE
 - PizzaPersonnalisee avec OIGNON, OEUF, TOMATE, FROMAGE et une pate EPAISSE

Rappel avec la TVA en pourcentage : prix TTC = prix HT * (1 + TVA/100)

## Un dernier mot

Créer un fichier **qui.txt** avec vos adresses mail à l'EPSI.
Renvoyer l'ensemble des sources et **qui.txt** dans un zip à l'adresse mail suivante :
  [nicolas.sanou@wijin.tech](mailto:nicolas.sanou@wijin.tech)

Merci pour votre participation !

